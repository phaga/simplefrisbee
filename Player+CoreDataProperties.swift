//
//  Player+CoreDataProperties.swift
//  
//
//  Created by Per Thomas Haga on 06.02.2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Player {

    @NSManaged var email: String?
    @NSManaged var name: String?
    @NSManaged var me: Me?
    @NSManaged var games: NSSet?
    @NSManaged var score: NSSet?

}
