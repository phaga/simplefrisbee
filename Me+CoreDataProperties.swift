//
//  Me+CoreDataProperties.swift
//  
//
//  Created by Per Thomas Haga on 02.02.2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Me {

    @NSManaged var name: String?
    @NSManaged var player: Player?

}
