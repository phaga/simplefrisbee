//
//  CourseTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 02.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

class CourseTableViewController: UITableViewController, AddCourseTableViewControllerDelegate, ChangeCourseTableViewControllerDelegate {
    
    var courses = [Course]()
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        loadData()
        tableView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        loadData()
        tableView.reloadData()
    }
    
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addCourse" {
            let destination = segue.destinationViewController as! UINavigationController
            let addCourseTableViewController = destination.topViewController as! AddCourseTableViewController
            
            addCourseTableViewController.delegate = self
        } else if segue.identifier == "changeCourse" {
            let destination = segue.destinationViewController as! UINavigationController
            let addCourseTableViewController = destination.topViewController as! ChangeCourseTableViewController
            let indexPath = tableView.indexPathForSelectedRow
            addCourseTableViewController.course = courses[(indexPath?.row)!]
        }
        
        
        
    }
    
    func didAddCourse() {
        self.loadData()
        tableView.reloadData()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func didChangeCourse() {
        self.loadData()
        tableView.reloadData()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func loadData() {
        let courseFetch = NSFetchRequest(entityName: "Course")
        
        do {
            let fetchedCourse = try managedContext.executeFetchRequest(courseFetch) as! [Course]
            courses = fetchedCourse
        } catch {
            fatalError("Bad things happened")
        }
        
        courses = courses.sort({($0).name < ($1).name})
    }

    // MARK: - Table view data source

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("courseCell", forIndexPath: indexPath)
        
        let thisCourse = courses[indexPath.row]

        cell.textLabel?.text = "\(thisCourse.name!) (\(thisCourse.numHoles!) hull)"
        

        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.courses.count
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }


    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // remove the deleted item from the model
            managedContext.deleteObject(courses[indexPath.row] as NSManagedObject)
            courses.removeAtIndex(indexPath.row)
            
            do {
                try managedContext.save()
            } catch {
                fatalError("Fatal error!")
            }
            
            // remove the deleted item from the `UITableView`
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("changeCourse", sender: nil)
    }
    

    

}
