//
//  Game+CoreDataProperties.swift
//  
//
//  Created by Per Thomas Haga on 18.02.2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Game {

    @NSManaged var date: NSDate?
    @NSManaged var totalParDiff: NSNumber?
    @NSManaged var totalScore: NSNumber?
    @NSManaged var numHoles: NSNumber?
    @NSManaged var course: Course?
    @NSManaged var history: NSSet?
    @NSManaged var players: NSSet?
    @NSManaged var score: NSSet?

}
