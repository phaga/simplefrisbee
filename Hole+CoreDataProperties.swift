//
//  Hole+CoreDataProperties.swift
//  
//
//  Created by Per Thomas Haga on 01.02.2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Hole {

    @NSManaged var length: NSNumber?
    @NSManaged var number: NSNumber?
    @NSManaged var par: NSNumber?
    @NSManaged var parDefined: NSNumber?
    @NSManaged var locked: NSNumber?
    @NSManaged var course: Course?
    @NSManaged var score: NSSet?

}
