//
//  Course.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 02.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation


class Course: NSManagedObject {

    func distanceFromLocation(location: CLLocation) -> Double {
        
        
        let thisLocation = CLLocation(latitude: self.lat as! CLLocationDegrees, longitude: self.long as! CLLocationDegrees)
        
        return location.distanceFromLocation(thisLocation)
    }

}
