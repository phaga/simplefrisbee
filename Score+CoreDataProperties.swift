//
//  Score+CoreDataProperties.swift
//  
//
//  Created by Per Thomas Haga on 06.02.2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Score {

    @NSManaged var holeNr: NSNumber?
    @NSManaged var score: NSNumber?
    @NSManaged var game: Game?
    @NSManaged var hole: Hole?
    @NSManaged var player: Player?

}
