//
//  Course+CoreDataProperties.swift
//  
//
//  Created by Per Thomas Haga on 14.01.2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Course {

    @NSManaged var name: String?
    @NSManaged var numHoles: NSNumber?
    @NSManaged var lat: NSNumber?
    @NSManaged var long: NSNumber?
    @NSManaged var holes: NSSet?
    @NSManaged var game: NSSet?
    @NSManaged var history: NSSet?

}
