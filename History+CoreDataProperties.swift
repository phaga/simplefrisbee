//
//  History+CoreDataProperties.swift
//  
//
//  Created by Per Thomas Haga on 14.01.2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension History {

    @NSManaged var date: NSDate?
    @NSManaged var course: Course?
    @NSManaged var game: Game?

}
