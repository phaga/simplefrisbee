//
//  ChangePlayerTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 30.12.2015.
//  Copyright © 2015 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

protocol ChangePlayerTableViewControllerDelegate {
    func didChangePlayer()
}

class ChangePlayerTableViewController: UITableViewController, UITextFieldDelegate {
    
    var player: NSManagedObject? {
        didSet {
            self.configureView()
        }
    }
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    @IBOutlet weak var nameTextField: UITextField?
    @IBOutlet weak var emailTextField: UITextField?
    @IBOutlet weak var meToggle: UISwitch?

    var delegate: ChangePlayerTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField?.delegate = self
        emailTextField?.delegate = self
        
        configureView()
        
        nameTextField?.becomeFirstResponder()

    }
    
    func isMe() -> Bool {
        let meFetch = NSFetchRequest(entityName: "Me")
        var fetchedMe = [Me]()
        do {
            fetchedMe = try managedContext.executeFetchRequest(meFetch) as! [Me]
            
        } catch {
            fatalError("Bad things happened")
        }
        
        if fetchedMe.first?.player == self.player {
            return true
        } else {
            return false
        }
        
    }
    
    func configureView() {
        if let pl = player as? Player {
            if let email = pl.email {
                self.emailTextField?.text = email
            }
            if isMe() {
                self.meToggle?.on = true
            } else {
                self.meToggle?.on = false
            }
            
        } else {
            print("Could not cast")
        }
        
        
        self.nameTextField?.text = (player as! Player).name
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        print("textFieldReturn!")
        if (textField == self.nameTextField) {
            self.emailTextField?.becomeFirstResponder()
        }
        else{
            saveButtonPressed(self)
        }
        
        return true
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        if let playerObject = player as? Player {
            playerObject.name = nameTextField?.text
            playerObject.email = emailTextField?.text
        }
        
        if meToggle!.on == true {
            // Check if me is defined
            let fetch = NSFetchRequest(entityName: "Me")
            
            do {
                let fetchedMe = try managedContext.executeFetchRequest(fetch) as! [Me]
                let me = fetchedMe
                
                if me.count > 0 {
                    // Redefine me
                    me.first?.player = self.player as? Player
                } else {
                    // Define me
                    let meEntity = NSEntityDescription.insertNewObjectForEntityForName("Me", inManagedObjectContext: managedContext) as! Me
                    meEntity.player = self.player as? Player
                }
            } catch {
                fatalError("Bad things happened")
            }
            
        } else {
            // Check if me is defined
            let fetch = NSFetchRequest(entityName: "Me")
            
            do {
                let fetchedMe = try managedContext.executeFetchRequest(fetch) as! [Me]
                let me = fetchedMe
                
                if me.count > 0 {
                    // Remove me
                    me.first?.player = nil
                }
            } catch {
                fatalError("Bad things happened")
            }
        }
        
        do {
            try managedContext.save()
        } catch {
            fatalError("Something very wrong")
        }
        
        
        delegate?.didChangePlayer()
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
