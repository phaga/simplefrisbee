//
//  NewGameTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 06.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class NewGameTableViewController: UITableViewController, CLLocationManagerDelegate, UISearchBarDelegate, UISearchDisplayDelegate {
    var courses = [Course]()
    var location: CLLocation?
    var uniqueCourseArray = [Course]()
    var locationManager = CLLocationManager()
    var historyArray = [History]()
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Download courses from discgolfnorge.no
        let downloadManager = DownloadManager()
        downloadManager.getCourses()
        
        
        // Find location
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        // Load custom courses from coredata
        loadCourses()
        loadHistory()
        
        tableView.reloadData()


    }
    
    override func viewDidAppear(animated: Bool) {
        loadHistory()
    }

    
    func loadHistory() {
        let historyFetch = NSFetchRequest(entityName: "History")
        
        do {
            let fetchedHistory = try managedContext.executeFetchRequest(historyFetch) as! [History]
            historyArray = fetchedHistory
        } catch {
            fatalError("Bad things happened")
        }
        
        // Only show unique courses in history list
        uniqueCourseArray.removeAll()
        for history in historyArray {
            if let course = history.course {
                if !uniqueCourseArray.contains(course) {
                    uniqueCourseArray.append(course)
                }
            }
        }
    }
    
    func loadCourses() {
        let courseFetch = NSFetchRequest(entityName: "Course")
        
        do {
            let fetchedCourse = try managedContext.executeFetchRequest(courseFetch) as! [Course]
            courses = fetchedCourse
        } catch {
            fatalError("Bad things happened")
        }
    }
    
    
    func sortByDistance() {
        if let loc = self.location {
            courses = courses.sort({$0.distanceFromLocation(loc) < $1.distanceFromLocation(loc)})
        }
        
    }
    

    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            if location == nil {
                return 0
            } else {
                return min(3, courses.count)
            }
        } else {
            return min(3, uniqueCourseArray.count)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.location = location
            //locationManager.stopUpdatingLocation()
            loadCourses()
            sortByDistance()
            tableView.reloadData()
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            if location == nil {
                return "🗺 I Nærheten (laster...)"
            } else {
                return "🗺 I Nærheten"
            }
        }
        
        if section == 1 {
            
            return "🕐 Sist brukt"
            
        } else {
            
            return ""
            
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            // Find nearest courses
            let cell = tableView.dequeueReusableCellWithIdentifier("nearbyCell", forIndexPath: indexPath)
            if self.location != nil {
                cell.textLabel?.text = courses[indexPath.row].name
            }
            return cell
        } else {
            // Last used courses
            let cell = tableView.dequeueReusableCellWithIdentifier("nearbyCell", forIndexPath: indexPath)
            cell.textLabel?.text = uniqueCourseArray[indexPath.row].name
            
            return cell
        }
    }
    
    
    // MARK: Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showGameSettings" {
            let destination = segue.destinationViewController as! GameSettingsTableViewController
            
            if let selectedIndPath = tableView.indexPathForSelectedRow {
                if selectedIndPath.section == 0 {
                    destination.course = courses[selectedIndPath.row]
                } else {
                    NSLog(selectedIndPath.row.description)
                    destination.course = uniqueCourseArray[selectedIndPath.row]
                }
                
            }
        }
    }
    
    
    

}
