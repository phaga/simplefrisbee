//
//  SummaryTableViewCell.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 12.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit

class SummaryTableViewCell: UITableViewCell {
    @IBOutlet weak var holeNrLabel: UILabel?
    @IBOutlet weak var scoreLabel: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
