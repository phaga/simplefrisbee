//
//  CustomCollectionViewCell.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 15.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit

@IBDesignable
class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var textLabel: UILabel?
    @IBOutlet weak var textView: UIView?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func setup() {
        self.layer.borderWidth = 0.0
        self.layer.borderColor = UIColor.whiteColor().CGColor
        self.textView?.layer.cornerRadius = 10.0
        self.hidden = false
        self.textView?.layer.zPosition = 1
    }
    
    override func prepareForReuse() {
        self.textLabel?.text = nil
        self.textLabel?.font = nil
        self.textLabel?.textColor = nil
        self.textView?.backgroundColor = nil
        self.hidden = false
        self.backgroundColor = nil
    }
}
