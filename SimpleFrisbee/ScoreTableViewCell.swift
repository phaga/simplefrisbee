//
//  ScoreTableViewCell.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 18.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit

class ScoreTableViewCell: UITableViewCell {
    @IBOutlet weak var rightLabel: UILabel?
    @IBOutlet weak var middleLabel: UILabel?
    @IBOutlet weak var leftLabel: UILabel?
    @IBOutlet weak var playerButton: UIButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    

}
