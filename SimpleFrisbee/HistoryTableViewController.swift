//
//  HistoryTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 13.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

class HistoryTableViewController: UITableViewController {
    
    var dateFormatter = NSDateFormatter()
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    var historyArray = [Game]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadHistory()
        tableView.reloadData()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewDidAppear(animated: Bool) {
        loadHistory()
        tableView.reloadData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sortByDate() {
        historyArray = historyArray.sort({$0.date?.timeIntervalSince1970 > $1.date?.timeIntervalSince1970})
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return historyArray.count
    }
    
    func loadHistory() {
        let historyFetch = NSFetchRequest(entityName: "Game")
        
        do {
            let fetchedHistory = try managedContext.executeFetchRequest(historyFetch) as! [Game]
            historyArray = fetchedHistory
        } catch {
            fatalError("Bad things happened")
        }
        
        sortByDate()
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("historyCell", forIndexPath: indexPath) as! HistoryTableViewCell
        let game = historyArray[indexPath.row]
        
        dateFormatter.dateFormat = "d.MM.yy"
        if let courseName = game.course?.name {
            cell.courseNameLabel?.text = courseName
        } else {
            cell.courseNameLabel?.text = "Bane slettet"
        }
        
        cell.dateLabel?.text = dateFormatter.stringFromDate(game.date!)
        cell.totalScoreLabel?.text = game.totalScore?.description
        
        var prefix: String {
            if Int(game.totalParDiff!) > 0 {
                return "+"
            } else if Int(game.totalParDiff!) <= 0 {
                return ""
            } else {
                return ""
            }
        }
        let greenColor = UIColor(red: 0, green: 128/255.0, blue: 0.0, alpha: 1.0)
        var color = UIColor()
        if prefix == "+" {
            color = UIColor.redColor()
        } else {
            color = greenColor
        }
        
        cell.totalParDiffLabel?.text = "(\(prefix)\((game.totalParDiff?.description)!))"
        cell.totalParDiffLabel?.textColor = color
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("showGame", sender: nil)
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let indexPath = tableView.indexPathForSelectedRow
        let destination = segue.destinationViewController as! ShowGameViewController
        
        destination.game = historyArray[(indexPath?.row)!]
        
    }

}
