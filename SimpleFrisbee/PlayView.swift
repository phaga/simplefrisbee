//
//  PlayView.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 03.02.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit

class PlayView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func setup() {
        //self.layer.borderWidth = 5.0
        self.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.layer.cornerRadius = 10.0
        //self.layer.zPosition = 1
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSizeZero
        self.layer.shadowRadius = 10
    }
    

}
