//
//  AddHoleTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 02.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

protocol AddHoleTableViewControllerDelegate {
    var course: Course? { get set }
    var holeDict: [Int: Int] { get set }
    func didAddHole(hole: Int, par: Int, length: Double?)
}

class AddHoleTableViewController: UITableViewController, UITextFieldDelegate {
    @IBOutlet weak var parTextField: UITextField?
    @IBOutlet weak var lengthTextField: UITextField?
    
    var holeNumber: Int = 1
    
    var delegate: AddHoleTableViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Hull \(holeNumber)"
        
        parTextField?.delegate = self
        lengthTextField?.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        //toolBar.translucent = true
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        parTextField?.becomeFirstResponder()
        let doneButton = UIBarButtonItem(title: "Neste", style: UIBarButtonItemStyle.Plain, target: self, action: "doneButtonPressed")
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Avbryt", style: UIBarButtonItemStyle.Plain, target: self, action: "closeKeyboard")
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        
        parTextField?.inputAccessoryView = toolBar
        lengthTextField?.inputAccessoryView = toolBar
    

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }


    func configureView() {
        self.title = "Hull \(holeNumber)"
        parTextField?.text = ""
        lengthTextField?.text = ""
    }
    
    func doneButtonPressed() {
        if let parText = parTextField {
            if parText.isFirstResponder() {
                textFieldShouldReturn(parTextField!)
            } else {
                textFieldShouldReturn(lengthTextField!)
            }
        }
        
    }
    
    func closeKeyboard() {
        lengthTextField?.resignFirstResponder()
        parTextField?.resignFirstResponder()
    }
    
    func getCourseName() -> String? {
        let thisCourse = self.delegate?.course
        
        return thisCourse!.name
    }
    
    func loadHole(hole: Int) -> Bool {
        return false
    }
    
    

    // MARK: - Table view data source
    
    
    @IBAction func saveButtonPressed(sender: AnyObject) {

        if let parString = parTextField?.text, let par = Int(parString) {
            var length: Double? = nil
            if lengthTextField != "" {
                length = Double((lengthTextField?.text)!)
            }
            self.delegate?.didAddHole(holeNumber, par: par, length: length)
            self.holeNumber += 1
            configureView()
        }
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func nextButtonPressed(sender: AnyObject) {
        if let parString = parTextField?.text, let par = Int(parString) {
            var length: Double? = nil
            if lengthTextField != "" {
                length = Double((lengthTextField?.text)!)
            }
            self.delegate?.didAddHole(holeNumber, par: par, length: length)
            self.holeNumber += 1
            configureView()
            parTextField?.becomeFirstResponder()
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == parTextField {
            lengthTextField?.becomeFirstResponder()
        } else {
            nextButtonPressed(self)
        }
        return true
    }
    
    @IBAction func prevButtonPressed(sender: AnyObject) {
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
         navigationController?.popViewControllerAnimated(true)
    }
    

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
