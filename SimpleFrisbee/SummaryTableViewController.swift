//
//  SummaryTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 12.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

protocol SummaryTableViewControllerDelegate {
    func didSaveGame()
}

class SummaryTableViewController: UITableViewController {
    
    var holeArray = [Hole]()
    var scoreDict = [Player: [Int]]()
    var penaltyArray = [Int]()
    var parDiffArray = [Int]()
    var playerArray = [Player]()
    var course: Course?
    var totalScore = 0
    var totalParDiff = 0
    
    var delegate: SummaryTableViewControllerDelegate?
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    @IBOutlet weak var totalScoreLabel: UILabel?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        computeParDiff()
        tableView.reloadData()
        
        totalScore = computeTotalScore()
        //let totalPar = computeTotalPar()
        totalParDiff = computeTotalParDiff()
        
        var parDiffSign = "+"
        if totalParDiff < 0 {
            parDiffSign = "-"
        }
        totalScoreLabel?.text = "\(totalScore) (\(parDiffSign) \(totalParDiff))"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return scoreDict[playerArray.first!]!.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("holeCell", forIndexPath: indexPath) as! SummaryTableViewCell
        
        
        var parDiffSign = "+"
        if parDiffArray[indexPath.row] < 0 {
            parDiffSign = "-"
        }
        
        cell.holeNrLabel?.text = (holeArray[indexPath.row]).number?.description
        cell.scoreLabel?.text = "\(scoreDict[playerArray.first!]![indexPath.row].description) (\(parDiffSign) \(abs(parDiffArray[indexPath.row])))"

        return cell
    }
    
    func computeParDiff() {
        var i = 0
        for score in scoreDict[playerArray.first!]! {
            parDiffArray.append(Int(score) - Int(holeArray[i].par!))
            i++
        }
    }
    
    func computeTotalParDiff() -> Int {
        var tot = 0
        for parDiff in parDiffArray {
            tot += parDiff
        }
        return tot
    }
    
    func computeTotalPar() -> Int {
        var tot = 0
        for hole in holeArray {
            tot += Int(hole.par!)
        }
        print(tot)
        return tot
        
    }
    
    func computeTotalScore() -> Int {
        var tot = 0
        for score in scoreDict[playerArray.first!]! {
            tot += score
        }
        print(tot)
        return tot
        
    }
    
    func saveGame() {
        let entity = NSEntityDescription.insertNewObjectForEntityForName("Game", inManagedObjectContext: managedContext) as! Game
        
        let thisDate = NSDate()
        entity.setValue(thisDate, forKey: "date")
        entity.setValue(course, forKey: "course")
        print(totalScore)
        entity.setValue(totalScore, forKey: "totalScore")
        entity.setValue(totalParDiff, forKey: "totalParDiff")
        entity.setValue(holeArray.count, forKey: "numHoles")
        
        var i = 1
        for score in scoreDict[playerArray.first!]! {
            let scoreEntity = NSEntityDescription.insertNewObjectForEntityForName("Score", inManagedObjectContext: managedContext) as! Score
            scoreEntity.setValue(score, forKey: "score")
            scoreEntity.setValue(i, forKey: "holeNr")
            scoreEntity.setValue(entity, forKey: "game")
            scoreEntity.setValue(holeArray[i-1], forKey: "hole")
            
            i++
        }
        
        let historyEntity = NSEntityDescription.insertNewObjectForEntityForName("History", inManagedObjectContext: managedContext) as! History
        
        historyEntity.setValue(thisDate, forKey: "date")
        historyEntity.setValue(course, forKey: "course")
        historyEntity.setValue(entity, forKey: "game")
        
        
        do {
            try managedContext.save()
        } catch {
            fatalError("Fatal Error!")
        }
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        saveGame()
        self.delegate?.didSaveGame()
        dismissViewControllerAnimated(true, completion: nil)
    }

    

}
