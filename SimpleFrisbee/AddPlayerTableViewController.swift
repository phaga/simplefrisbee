//
//  AddPlayerTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 30.12.2015.
//  Copyright © 2015 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

protocol AddPlayerTableViewControllerDelegate {
    func didSaveNewPlayer()
}

class AddPlayerTableViewController: UITableViewController, UITextFieldDelegate {
    @IBOutlet weak var nameTextField: UITextField?
    @IBOutlet weak var emailTextField: UITextField?
    @IBOutlet weak var meToggle: UISwitch?
    
    var delegate: AddPlayerTableViewControllerDelegate?
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField?.delegate = self
        emailTextField?.delegate = self
        
        nameTextField?.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        savePerson()
        delegate?.didSaveNewPlayer()
    }
    
    func savePerson() {
        
        let entity = NSEntityDescription.insertNewObjectForEntityForName("Player", inManagedObjectContext: managedContext) as! Player
        
        if let name = nameTextField?.text {
            entity.setValue(name, forKey: "name")
            entity.setValue(emailTextField?.text, forKey: "email")
        }
        
        if meToggle!.on == true {
            // Check if me is defined
            let fetch = NSFetchRequest(entityName: "Me")
            
            do {
                let fetchedMe = try managedContext.executeFetchRequest(fetch) as! [Me]
                let me = fetchedMe
                
                if me.count > 0 {
                    // Redefine me
                    me.first?.player = entity
                } else {
                    // Define me
                    let meEntity = NSEntityDescription.insertNewObjectForEntityForName("Me", inManagedObjectContext: managedContext) as! Me
                    meEntity.player = entity
                }
            } catch {
                fatalError("Bad things happened")
            }
            
        }
        
        // Save
        do {
            try managedContext.save()
        } catch {
            fatalError("Fatal Error!")
        }
        
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField == self.nameTextField) {
            self.emailTextField?.becomeFirstResponder()
        }
        else{
            saveButtonPressed(self)
        }
        
        return true
    }


}
