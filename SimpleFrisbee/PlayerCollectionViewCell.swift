//
//  PlayerCollectionViewCell.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 19.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit

class PlayerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var playerLabel: UILabel?
    @IBOutlet weak var playerView: UIView?
    @IBOutlet weak var totalScore: UILabel?
    @IBOutlet weak var parDiff: UILabel?
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func setup() {
        self.layer.borderWidth = 0
        self.layer.borderColor = UIColor.whiteColor().CGColor
        //self.layer.cornerRadius = 5.0
    }
    
    override func prepareForReuse() {
        playerLabel?.text = nil
        playerView?.backgroundColor = UIColor.blueColor()
        totalScore?.text = nil
        parDiff?.text = nil
        
    }
}
