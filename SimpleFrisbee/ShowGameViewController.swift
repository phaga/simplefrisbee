//
//  SummaryViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 05.02.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

class ShowGameViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource {
    var holeArray = [Hole]()
    var scoreDict = [Player: [Int]]()
    var playerArray  = [Player]()
    var parDiff = [Player: Int]()
    var course: Course?
    var showDetailPlayer: Player?
    var avgPerHole = [Player: Double]()
    
    var game: Game?
    
    @IBOutlet weak var detailView: PlayView?
    @IBOutlet weak var detailTableView: UITableView?
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    
    @IBOutlet weak var playView: PlayView?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var playerNameLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadGame()
        
        detailView?.hidden = true
        collectionView?.delegate = self
        collectionView?.dataSource = self
        detailTableView?.delegate = self
        detailTableView?.dataSource = self
        collectionHeightConstraint?.constant = CGFloat(playerArray.count*50 + 64)
        showDetailPlayer = playerArray.first!
        configureDetails()
        
        
        
        playerArray = playerArray.sort({computeTotal(forPlayer: $0) < computeTotal(forPlayer: $1)})
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadGame() {
        playerArray = (game?.players?.allObjects) as! [Player]
        let scores = (game?.score?.allObjects) as! [Score]
        
        for player in playerArray {
            scoreDict[player] = [Int](count: Int((game?.numHoles!)!), repeatedValue: 0)
        }
        
        for score in scores {
            let player = score.player
            scoreDict[player!]?[Int(score.holeNr!) - 1] = Int(score.score!)
            
            if player == playerArray.first! {
                holeArray.append(score.hole!)
            }
        }
        
        
    }
    
    func toggleShowDetail() {
        let slideInFromLeftTransition = CATransition()
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromBottom
        slideInFromLeftTransition.duration = 0.3
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        // Add the animation to the View's layer
        detailView!.layer.addAnimation(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        
        if detailView?.hidden == true {
            detailView?.hidden = false
        } else {
            detailView?.hidden = true
        }
    }
    
    func showDetail(forPlayer player: Player) {
        let slideInFromLeftTransition = CATransition()
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromBottom
        slideInFromLeftTransition.duration = 0.3
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        // Add the animation to the View's layer
        detailView!.layer.addAnimation(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        playerNameLabel?.text = showDetailPlayer?.name
        detailView?.hidden = false
        detailTableView?.reloadData()
    }
    
    func computeTotal(forPlayer player: Player) -> Int? {
        guard let scores = scoreDict[player] else {
            return nil
        }
        var tot = 0
        for score in scores {
            tot += score
        }
        
        return tot
    }
    
    func configureDetails() {
        for player in playerArray {
            if let totalScore = computeTotal(forPlayer: player) {
                avgPerHole[player] = Double(totalScore) / Double(holeArray.count)
                
            }
        }
    }
    
    
    func saveGame() {
        let gameEntity = NSEntityDescription.insertNewObjectForEntityForName("Game", inManagedObjectContext: managedContext) as! Game
        
        let thisDate = NSDate()
        let players = NSSet(array: playerArray)
        gameEntity.setValue(players, forKey: "players")
        gameEntity.setValue(thisDate, forKey: "date")
        gameEntity.setValue(course, forKey: "course")
        
        
        
        var i = 1
        for player in playerArray {
            for score in scoreDict[player]! {
                let scoreEntity = NSEntityDescription.insertNewObjectForEntityForName("Score", inManagedObjectContext: managedContext) as! Score
                scoreEntity.setValue(score, forKey: "score")
                scoreEntity.setValue(i, forKey: "holeNr")
                scoreEntity.setValue(gameEntity, forKey: "game")
                scoreEntity.setValue(holeArray[i-1], forKey: "hole")
                scoreEntity.setValue(player, forKey: "player")
                
                i++
            }
        }
        
        
        
        let historyEntity = NSEntityDescription.insertNewObjectForEntityForName("History", inManagedObjectContext: managedContext) as! History
        
        historyEntity.setValue(thisDate, forKey: "date")
        historyEntity.setValue(course, forKey: "course")
        historyEntity.setValue(gameEntity, forKey: "game")
        
        
        do {
            try managedContext.save()
        } catch {
            fatalError("Fatal Error!")
        }
        
        navigationController?.popToRootViewControllerAnimated(true)
        
    }
    
    // MARK: - Collection Source functions
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return playerArray.count + 1
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.item == 0 {
            
            self.showDetailPlayer = playerArray[indexPath.section - 1]
            
            showDetail(forPlayer: playerArray[indexPath.section - 1])
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 && indexPath.section == 0 {
            // Player cell
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("summaryHeaderCell", forIndexPath: indexPath) as! HeaderCollectionViewCell
            
            cell.mainTitle?.text = "Spiller"
            cell.subTitle?.text = ""
            
            return cell
        } else if indexPath.item == 0 && indexPath.section > 0 {
            // Player cell
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("playerCell", forIndexPath: indexPath) as! PlayerCollectionViewCell
            
            cell.playerView?.backgroundColor = ColorPalette.blueColor.color
            
            cell.playerLabel?.text = playerArray[Int(indexPath.section) - 1].name!
            
            return cell
        } else if indexPath.item == 1 && indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("summaryHeaderCell", forIndexPath: indexPath) as! HeaderCollectionViewCell
            cell.mainTitle?.text = "Total"
            cell.subTitle?.text = ""
            return cell
        } else if indexPath.item == 2 && indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("summaryHeaderCell", forIndexPath: indexPath) as! HeaderCollectionViewCell
            cell.mainTitle?.text = "Par"
            cell.subTitle?.text = ""
            return cell
            
        }else if indexPath.item == 3 && indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("summaryHeaderCell", forIndexPath: indexPath) as! HeaderCollectionViewCell
            cell.mainTitle?.text = "Bedring"
            cell.subTitle?.text = "fra siste spill"
            return cell
        }  else if indexPath.item == 1 && indexPath.section > 0 {
            // Print totals
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
            cell.textLabel?.text = computeTotal(forPlayer: playerArray[indexPath.section - 1])?.description
            return cell
            
        } else if indexPath.item == 3 && indexPath.section > 0 {
            // Print par difference
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
            
            //cell.textLabel?.text = parDiff[playerArray[indexPath.section - 1]]!.description
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
            //cell.textLabel?.text = ""
            return cell
        }
        
        
    }
    
    // MARK: - Table data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var descriptionText = ""
        var rightDetail = ""
        
        switch (indexPath.row) {
        case 0:
            descriptionText = "Gjennomsnitt per kurv"
            rightDetail = String(format: "%.2f", avgPerHole[showDetailPlayer!]!)
        case 1: descriptionText = "Forrige spill på denne banen"
        case 2: descriptionText = "    ► differanse"
        case 3: descriptionText = "Gjennomsnitt på denne banen"
        case 4: descriptionText = "    ► differanse"
            
        default:
            descriptionText = ""
            rightDetail = ""
        }
        let cell = detailTableView?.dequeueReusableCellWithIdentifier("detailCell")
        cell?.textLabel?.text = descriptionText
        cell?.detailTextLabel?.text = rightDetail
        
        return cell!
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
