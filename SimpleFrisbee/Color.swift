//
//  Color.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 29.12.2015.
//  Copyright © 2015 Per Thomas Haga. All rights reserved.
//

import UIKit

enum ColorPalette {
    case mainColor
    case secondaryColor
    case darkColor
    case lightColor
    case lightBlue
    case grayColor
    case blueColor
    case greenColor
    case skyBlueColor
    case viewBackGroundColor
    
    var color: UIColor {
        switch self {
        case .mainColor: return UIColor(red: 52.0/255.0, green: 73.0/255.0, blue: 94.0/255.0, alpha: 1)
        case .secondaryColor: return UIColor(red: 179.0/255.0, green: 91.0/255.0, blue: 82.0/255.0, alpha: 1)
        case .darkColor: return UIColor(red: 43.0/255.0, green: 43.0/255.0, blue: 43.0/255.0, alpha: 1)
        case .lightColor: return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        case .lightBlue: return UIColor(red: 227/255.0, green: 247/255.0, blue: 255/255.0, alpha: 1)
        case .grayColor: return UIColor(red: 206/255.0, green: 204/255.0, blue: 216/255.0, alpha: 1)
        case .blueColor: return UIColor(red: 74/255.0, green: 144/255.0, blue: 226/255.0, alpha: 1)
            
        case .greenColor: return UIColor(red: 0.0/255.0, green: 128.0/255.0, blue: 0.0/255.0, alpha: 1)
        case .skyBlueColor: return UIColor(red: 164/255.0, green: 205/255.0, blue: 255/255.0, alpha: 1)
        case .viewBackGroundColor: return UIColor(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1)
        }
        
    }
}

let apiKey = "AIzaSyAPy9a-kJtwp_hFMjZgj7Eq8cbLDB8jZzg"



