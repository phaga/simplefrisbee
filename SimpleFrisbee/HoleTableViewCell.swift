//
//  HoleTableViewCell.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 02.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit

class HoleTableViewCell: UITableViewCell {
    @IBOutlet weak var holeNrLabel: UILabel?
    @IBOutlet weak var parLabel: UILabel?
    @IBOutlet weak var lengthLabel: UILabel?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
