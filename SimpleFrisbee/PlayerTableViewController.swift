//
//  PlayerTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 30.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

class PlayerTableViewController: UITableViewController, AddPlayerTableViewControllerDelegate, ChangePlayerTableViewControllerDelegate {
    
    var players = [Player]()
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        tableView.reloadData()
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSaveNewPlayer() {
        loadData()
        tableView.reloadData()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func didChangePlayer() {
        NSLog("Did change player")
        navigationController?.popViewControllerAnimated(true)
        loadData()
        tableView.reloadData()
    }
    
    func loadData() {
        let playerFetch = NSFetchRequest(entityName: "Player")
        
        do {
            let fetchedPlayers = try managedContext.executeFetchRequest(playerFetch) as! [Player]
            players = fetchedPlayers
        } catch {
            fatalError("Bad things happened")
        }
        
        players = players.sort({$0.name < $1.name})
    }
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("playerCell", forIndexPath: indexPath)
        
        
        cell.textLabel?.text = "\(players[indexPath.row].name!)"

        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // remove the deleted item from the model
            managedContext.deleteObject(players[indexPath.row] as NSManagedObject)
            players.removeAtIndex(indexPath.row)
            
            do {
                try managedContext.save()
            } catch {
                fatalError("Fatal error!")
            }
            
            // remove the deleted item from the `UITableView`
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showAddPlayerView" {
            let destination = segue.destinationViewController as! UINavigationController
            let addPlayerTableViewController = destination.topViewController as! AddPlayerTableViewController
            addPlayerTableViewController.delegate = self
        } else if segue.identifier == "showPlayerDetail" {
            let destination = segue.destinationViewController as! ChangePlayerTableViewController
            destination.delegate = self
            destination.player = players[(tableView.indexPathForSelectedRow?.row)!]
            
        }
    }


}
