//
//  AddDiscTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 31.12.2015.
//  Copyright © 2015 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

protocol AddDiscTableViewControllerDelegate {
    func didSaveNewDisc()
}

class AddDiscTableViewController: UITableViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var nameTextField: UITextField?
    @IBOutlet weak var typeTextField: UITextField?
    @IBOutlet weak var speedTextField: UITextField?
    @IBOutlet weak var turnTextField: UITextField?
    @IBOutlet weak var glideTextField: UITextField?
    @IBOutlet weak var fadeTextField: UITextField?
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    var delegate: AddDiscTableViewControllerDelegate?
    
    let types: [String] = ["Driver", "Midrange", "Putter"]
    let speeds: [Int] = Array(1...13)
    let glides: [Int] = Array(1...7)
    let turns: [Int] = Array(-5...1)
    let fades: [Int] = Array(0...5)
    
    let typePicker = UIPickerView()
    let speedPicker = UIPickerView()
    let turnPicker = UIPickerView()
    let fadePicker = UIPickerView()
    let glidePicker = UIPickerView()
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pickers = [
            typePicker,
            speedPicker,
            turnPicker,
            fadePicker,
            glidePicker
        ]
        
        let textFields = [
            nameTextField,
            typeTextField,
            speedTextField,
            turnTextField,
            glideTextField,
            fadeTextField
        ]
        
        for textField in textFields {
            textField?.delegate = self
        }
        
        for picker in pickers {
            picker.delegate = self
            picker.dataSource = self
        }
        
        
        typePicker.showsSelectionIndicator = true
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        //toolBar.translucent = true
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Neste", style: UIBarButtonItemStyle.Plain, target: self, action: "nextField")
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Avbryt", style: UIBarButtonItemStyle.Plain, target: self, action: "closePicker")
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        
        for textField in textFields {
            textField?.inputAccessoryView = toolBar
        }
        
        
        typeTextField?.inputView = typePicker
        speedTextField?.inputView = speedPicker
        turnTextField?.inputView = turnPicker
        fadeTextField?.inputView = fadePicker
        glideTextField?.inputView = glidePicker
        
        nameTextField?.becomeFirstResponder()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField.text == "" {
            textField.text = ""
        }
    }
    
    func nextField() {
        if let nameText = nameTextField,
            let typeText = typeTextField,
            let speedText = speedTextField,
            let turnText = turnTextField,
            let glideText = glideTextField {
            
                if nameText.isFirstResponder() {
                    typeTextField?.becomeFirstResponder()
                } else if typeText.isFirstResponder() {
                    speedTextField?.becomeFirstResponder()
                } else if speedText.isFirstResponder() {
                    turnTextField?.becomeFirstResponder()
                } else if turnText.isFirstResponder() {
                    glideTextField?.becomeFirstResponder()
                } else if glideText.isFirstResponder() {
                    fadeTextField?.becomeFirstResponder()
                }
            }
    }
    
    func closePicker() {
        view.endEditing(true)
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == typeTextField ||
            textField == speedTextField ||
            textField == turnTextField ||
            textField == glideTextField ||
            textField == fadeTextField {
            return false
        }
        else {
            return true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == typePicker {
            return types.count + 1
        }
        if pickerView == speedPicker {
            return speeds.count + 1
        }
        if pickerView == turnPicker {
            return turns.count + 1
        }
        if pickerView == fadePicker {
            return fades.count + 1
        }
        if pickerView == glidePicker {
            return glides.count + 1
        } else {
            return 0
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "Ikke valgt"
        } else if pickerView == typePicker {
            return "\(types[row - 1])"
        } else if pickerView == speedPicker {
            return "\(speeds[row - 1])"
        } else if pickerView == turnPicker {
            return "\(turns[row - 1])"
        } else if pickerView == fadePicker {
            return "\(fades[row - 1])"
        } else if pickerView == glidePicker {
            return "\(glides[row - 1])"
        } else {
            return nil
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == typePicker {
            if row == 0 {
                typeTextField?.text = ""
            } else {
                typeTextField?.text = types[row - 1]
            }
        } else if pickerView == speedPicker {
            if row == 0 {
                speedTextField?.text = ""
            } else {
                speedTextField?.text = "Speed: \(speeds[row - 1])"
            }
        } else if pickerView == turnPicker {
            if row == 0 {
                turnTextField?.text = ""
            } else {
                turnTextField?.text = "Turn: \(turns[row - 1])"
            }
        } else if pickerView == fadePicker {
            if row == 0 {
                fadeTextField?.text = ""
            } else {
                fadeTextField?.text = "Fade: \(fades[row - 1])"
            }
        } else if pickerView == glidePicker {
            if row == 0 {
                glideTextField?.text = ""
            } else {
                glideTextField?.text = "Glide: \(glides[row - 1])"
            }
        }
        
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == nameTextField {
            textField.resignFirstResponder()
            typeTextField?.becomeFirstResponder()
        }
        return true
    }
    
    
    @IBAction func saveButtonPressed(sender: UIBarButtonItem) {
        saveDisc()
        self.delegate?.didSaveNewDisc()
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func saveDisc() {
        
        let entity = NSEntityDescription.insertNewObjectForEntityForName("Disc", inManagedObjectContext: managedContext) as! Disc
        
        if let name = nameTextField?.text {
            entity.setValue(name, forKey: "name")
            entity.setValue(typeTextField?.text, forKey: "type")
        }
        
        
        do {
            try managedContext.save()
        } catch {
            fatalError("Fatal Error!")
        }
        
    }

}
