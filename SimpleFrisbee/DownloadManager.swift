//
//  DownloadManager.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 21.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import Foundation
import CoreData

class DownloadManager {
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    var courses = [Course]()
    
    
    lazy var config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
    lazy var session: NSURLSession = NSURLSession(configuration: self.config)
    
    
    // URL to JSON object
    let queryURL = NSURL(string: "http://www.discgolfpark.no/baner-butikker/kart.html?option=com_hotspots&view=jsonv4&task=gethotspots&hs-language=nb-NO&page=1&per_page=100&cat=11%3B8%3B9%3B10&level=4&ne=74.338897%2C42.327402&sw=50.475866%2C-13.746816&c=64.935765%2C14.290293&fs=0&offset=0&format=raw")
    
    
    var error: String?
    
    typealias JSONDictionaryCompletion = ([String: AnyObject]?) -> Void
    
    func downloadJSONFromURL(completion: JSONDictionaryCompletion) {
        
        // Making request from server
        let request: NSURLRequest = NSURLRequest(URL: queryURL!)
        
        
        let dataTask = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            // 1. Check HTTP response for successful GET request
            
            if let httpResponse = response as? NSHTTPURLResponse {
                
                switch(httpResponse.statusCode) {
                case 200:
                    // 2. Create JSON object with data
                    do {
                        let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: [NSJSONReadingOptions.AllowFragments, NSJSONReadingOptions.MutableContainers]) as? [String: AnyObject]
                        completion(jsonDictionary)
                        //print(jsonDictionary?["items"])
                        
                    } catch let error as NSError {
                        print("Error: \(error)")
                    }
                    
                    
                default:
                    self.error = "GET request not successful. HTTP status code: \(httpResponse.statusCode)"
                    print("GET request not successful. HTTP status code: \(httpResponse.statusCode)")
                }
                
            } else {
                print("Error: Not a valid HTTP response.")
            }
            
            
        }
        dataTask.resume()
    }
    
    func downloadParJSON(forCourse course: String, completion: JSONDictionaryCompletion) {
        
        // Making request from server
        
        guard let queryURL = NSURL(string: "http://pthaga.no/frisbee/getParForCourse.php?course=" + course.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!) else {
            return
        }
        
        
        
        print(queryURL)
        let request: NSURLRequest = NSURLRequest(URL: queryURL)

        
        
        
        let dataTask = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            // 1. Check HTTP response for successful GET request
            
            if let httpResponse = response as? NSHTTPURLResponse {
                
                switch(httpResponse.statusCode) {
                case 200:
                    // 2. Create JSON object with data
                    do {
                        let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: [NSJSONReadingOptions.AllowFragments]) as? [String: AnyObject]
                        completion(jsonDictionary)
                        //print(jsonDictionary?["items"])
                        
                    } catch let error as NSError {
                        print("Error: \(error)")
                    }
                    
                    
                default:
                    self.error = "GET request not successful. HTTP status code: \(httpResponse.statusCode)"
                    print("GET request not successful. HTTP status code: \(httpResponse.statusCode)")
                }
                
            } else {
                print("Error: Not a valid HTTP response.")
            }
            
            
        }
        dataTask.resume()
    }
    
    
    func courseExists(name: String) -> Bool {
        let courseFetch = NSFetchRequest(entityName: "Course")
        let predicate1 = NSPredicate(format: "name == %@", name)
        let compoundPredicate = NSCompoundPredicate(type: NSCompoundPredicateType.OrPredicateType, subpredicates: [predicate1])
        courseFetch.predicate = compoundPredicate
        var error: NSError? = nil
        let num = managedContext.countForFetchRequest(courseFetch, error: &error)
        if num > 0 {
            return true
        } else {
            return false
        }
    }
    
    func getPar(forCourse course: Course) {
        // Checks if a par exists for the course, and adds it to Core data if it does.
        
        downloadParJSON(forCourse: course.name!) {
            
            (let JSONDict) in
            dispatch_async(dispatch_get_main_queue()) {
                guard let dict = JSONDict else {
                    return
                }
                var holes = course.holes?.allObjects as! [Hole]
                holes = holes.sort({Int($0.number!) < Int($1.number!)})
                
                
                for holeWithPar in dict {
                    holes[Int(holeWithPar.0)! - 1].setValue(Int(holeWithPar.1["par"]! as! String)!, forKey: "par")
                    holes[Int(holeWithPar.0)! - 1].setValue(true, forKey: "parDefined")
                    holes[Int(holeWithPar.0)! - 1].setValue(Int(holeWithPar.1["locked"]! as! String), forKey: "locked")
                    
                }
            }
        }
    }
    
    
    func getCourses() {
        downloadJSONFromURL{
            
            (let jSONDict) in
            
            dispatch_async(dispatch_get_main_queue()) {

                // let courseArray = jSONDict?["items"] as! NSMutableArray
                
                for course in jSONDict?["items"] as! [Dictionary<String, AnyObject>] {
                //for course in courseArray {
                    
                    let courseDict = course
                    
                    guard let nameText = courseDict["title"] else {
                        fatalError("Course did not have name.")
                    }
                    
                    // Don't load if course already exists
                    if self.courseExists(nameText as! String) || nameText.rangeOfString("G-Sport").location < 10000 || nameText.rangeOfString("G-Max").location < 10000 {
                        continue
                    }
                    
                    
                    let entity = NSEntityDescription.insertNewObjectForEntityForName("Course", inManagedObjectContext: self.managedContext) as! Course
                    
                    // Setting name
                    if let nameText = courseDict["title"] {
                        entity.setValue(nameText, forKey: "name")
                    }
                    
                    // Getting coordinates
                    if let lat = courseDict["lat"], long = courseDict["lng"] {
                        entity.setValue(lat, forKey: "lat")
                        entity.setValue(long, forKey: "long")
                    }
                    
                    // Getting description
                    let description = courseDict["description"] as! NSString
                    
                    // Looking for "Hull: " in description to get the number of baskets in course
                    let stringRange = description.rangeOfString("Hull: ")
                    
                    if stringRange.location < 10000 {
                        
                        // "Hull: " not found in string
                        
                        var holeRange = NSMakeRange(stringRange.location + stringRange.length, 2)
                        var numHoles = Int(description.substringWithRange(holeRange))
                        if numHoles == nil {
                            holeRange = NSMakeRange(stringRange.location + stringRange.length, 1)
                            numHoles = Int(description.substringWithRange(holeRange))

                        }
                        
                        // Add the right number of holes. Default par 3.
                        for i in 1...numHoles! {
                            let holeEntity = NSEntityDescription.insertNewObjectForEntityForName("Hole", inManagedObjectContext: self.managedContext) as! Hole
                            
                            holeEntity.setValue(3, forKey: "par")
                            holeEntity.setValue(300, forKey: "length")
                            holeEntity.setValue(i, forKey: "number")
                            holeEntity.course = entity
                            holeEntity.setValue(false, forKey: "parDefined")
                        }
                        entity.setValue(numHoles, forKey: "numHoles")
                        self.getPar(forCourse: entity)

                    }
                    
                    
                                    
                    
                }
                do {
                    try self.managedContext.save()
                } catch {
                    print("Could not download courses.")
                }
                
                
            }
        }
    }
}
