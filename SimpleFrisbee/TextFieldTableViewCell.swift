//
//  TextFieldTableViewCell.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 01.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit

protocol TextFieldTableViewCellDelegate {
    func didChangeTextField(name: String)
}

class TextFieldTableViewCell: UITableViewCell {
    
    var delegate: TextFieldTableViewCellDelegate?
    
    @IBOutlet weak var textField: UITextField?
    
    @IBAction func didChangeTextField(sender: AnyObject) {
        self.delegate?.didChangeTextField((textField?.text)!)
    }

    override func awakeFromNib() {
        textField!.addTarget(self, action: Selector("didChangeTextField:"), forControlEvents: UIControlEvents.EditingChanged)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
