//
//  DiscTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 31.12.2015.
//  Copyright © 2015 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

class DiscTableViewController: UITableViewController, AddDiscTableViewControllerDelegate {
    
    var discs = [NSManagedObject]()
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Show edit button
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        
        loadDiscs()
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadDiscs() {
        let discFetch = NSFetchRequest(entityName: "Disc")
        
        do {
            let fetchedDiscs = try managedContext.executeFetchRequest(discFetch) as! [Disc]
            discs = fetchedDiscs
        } catch {
            fatalError("Bad things happened")
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return discs.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("discTableCell", forIndexPath: indexPath)
        let thisDisc = self.discs[indexPath.row] as! Disc
        cell.textLabel?.text = thisDisc.name
        cell.detailTextLabel?.text = thisDisc.type

        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            managedContext.deleteObject(discs[indexPath.row] as NSManagedObject)
            discs.removeAtIndex(indexPath.row)
            do {
                try managedContext.save()
            } catch {
                fatalError("Fatal error!")
            }

            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    func configureView() {
        let discFetch = NSFetchRequest(entityName: "Disc")
        
        do {
            let fetchedDisc = try managedContext.executeFetchRequest(discFetch) as! [Disc]
            discs = fetchedDisc
        } catch {
            fatalError("Bad things happened")
        }
        
        tableView.reloadData()
    }
    
    func didSaveNewDisc() {
        configureView()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addDisc" {
            let destination = segue.destinationViewController as! UINavigationController
            let addDiscTableViewController = destination.topViewController as! AddDiscTableViewController
            
            addDiscTableViewController.delegate = self
        }
        
        if segue.identifier == "changeDisc" {
            
            
            
        }
    }

}
