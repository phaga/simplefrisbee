//
//  HeaderCollectionViewCell.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 05.02.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit

@IBDesignable
class HeaderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var mainTitle: UILabel?
    @IBOutlet weak var subTitle: UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func setup() {
        self.layer.borderWidth = 0.0
        self.layer.borderColor = UIColor.whiteColor().CGColor
        self.hidden = false
        self.layer.zPosition = 1
    }
    
    override func prepareForReuse() {
        self.mainTitle?.text = nil
        self.subTitle?.font = nil
        self.hidden = false
        self.backgroundColor = nil
    }
}
