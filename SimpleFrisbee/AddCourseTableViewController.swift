//
//  AddCourseTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 01.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

protocol AddCourseTableViewControllerDelegate {
    func didAddCourse()
}

class AddCourseTableViewController: UITableViewController, AddHoleTableViewControllerDelegate, MapViewControllerDelegate {


    var holes = [NSManagedObject]()
    var totalPar = 0
    var totalLength = 0.0
    var delegate: AddCourseTableViewControllerDelegate?
    var position: CLLocation?
    
    
    var holeDict = [Int: Int]() {
        didSet {
            computeTotalPar()
        }
    }
    
    var lengthDict = [Int: Double]() {
        didSet {
            //computeTotalLength()
        }
    }
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    var course: Course?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func didSavePosition(position: CLLocation) {
        print("Did save position!")
        dismissViewControllerAnimated(true, completion: nil)
        
        self.position = position
        
        self.tableView.reloadData()
        
        
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1{
            return 1
        } else if section == 2 {
            return holeDict.count + 1
        } else {
            return 0
        }
    }
    
    func getCourseName() -> String? {
        let thisCourse = self.course
        
        return thisCourse!.name
    }
    
    func computeTotalPar() {
        for value in holeDict.values {
            self.totalPar += value
        }
    }
    
    func computeTotalLength() {
        for value in lengthDict.values {
            self.totalLength += value
        }
    }
    
    
    func saveCourse() {
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        guard let nameCell = tableView.cellForRowAtIndexPath(indexPath) as! TextFieldTableViewCell? else {
            return
        }
        let entity = NSEntityDescription.insertNewObjectForEntityForName("Course", inManagedObjectContext: managedContext) as! Course
        
        // Setting name
        if let nameText = nameCell.textField?.text {
            print(nameText)
            entity.setValue(nameText, forKey: "name")
        }
        
        if let pos = self.position {
            let lat = pos.coordinate.latitude
            let long = pos.coordinate.longitude
            entity.setValue(lat, forKey: "lat")
            entity.setValue(long, forKey: "long")
        }
        
        entity.setValue(holeDict.count, forKey: "numHoles")
        
        // Setting par and length
        for key in holeDict.keys {
            let holeEntity = NSEntityDescription.insertNewObjectForEntityForName("Hole", inManagedObjectContext: managedContext) as! Hole
            holeEntity.setValue(holeDict[key], forKey: "par")
            if let length = lengthDict[key] {
                holeEntity.setValue(length, forKey: "length")
            }
            holeEntity.setValue(key, forKey: "number")
            holeEntity.course = entity
        }
        
        
        do {
            try managedContext.save()
        } catch {
            fatalError("Fatal Error!")
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 2 {
            return CGFloat(44)
        }
        else {
            return CGFloat(44)
        }
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("textFieldCell") as! TextFieldTableViewCell
            cell.textField!.placeholder = "Navn"
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("placementCell")!
            if self.position != nil {
                cell.textLabel?.text = "✅Posisjon satt"
                cell.textLabel?.textColor = UIColor.blackColor()
            }
            return cell
        } else if indexPath.section == 2 {
            if indexPath.row == holeDict.count {
                let cell = tableView.dequeueReusableCellWithIdentifier("addHoleCell")!
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCellWithIdentifier("holeCell") as! HoleTableViewCell
                if let thisHole = holeDict[indexPath.row + 1] {
                    cell.holeNrLabel?.text = "\(indexPath.row + 1)"
                    cell.parLabel?.text = "Par \(thisHole)"
                    if let length = lengthDict[indexPath.row + 1] {
                        cell.lengthLabel?.text = "\(length) m"
                    } else {
                        cell.lengthLabel?.text = " - "
                    }
                }
                return cell
            }
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("holeCell")!
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch(section) {
        case 0: return "Navn"
        case 1: return "Sted"
        case 2: return "Hull"
        default: return nil
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addHole" {
            let destination = segue.destinationViewController as? AddHoleTableViewController
            destination?.delegate = self
            destination?.holeNumber = holeDict.count + 1
        } else if segue.identifier == "showMap" {
            let destination = segue.destinationViewController as! UINavigationController
            let addPositionTableViewController = destination.topViewController as! MapViewController
            print("show map!")
            addPositionTableViewController.delegate = self
            
        }
    }
    
    func didAddHole(hole: Int, par: Int, length: Double?) {
        self.holeDict.updateValue(par, forKey: hole)
        if let len = length {
            self.lengthDict.updateValue(len, forKey: hole)
        }
        print(holeDict)
        tableView.reloadData()
        
    }
    
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        self.saveCourse()
        self.delegate?.didAddCourse()
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }


}
