//
//  PlayViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 06.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class PlayViewController: UIViewController, SummaryTableViewControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var scoreStepper: UIStepper?
    @IBOutlet weak var scoreLabel: UILabel?
    @IBOutlet weak var nextHoleButton: UIButton?
    @IBOutlet weak var prevHoleButton: UIButton?
    @IBOutlet weak var headerLabel: UILabel?
    @IBOutlet weak var progressBar: UIProgressView?
    @IBOutlet weak var userButton: UIButton?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var parLabel: UILabel?
    @IBOutlet weak var courseNameLabel: UILabel?
    @IBOutlet weak var changeParButton: UIButton?
    @IBOutlet weak var parTextField: UITextField?
    @IBOutlet weak var addParButton: UIButton?

    
    var numHoles = 18
    var beginOnHole = 1
    var holeNumber = 1
    var totScore = [Player: Int]()
    var didPlayHole = [Player: [Bool]]()
    var parDiff = [Player: Int]()
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    var parArray: [NSManagedObject] {
        return [NSManagedObject]()
        
    }
    
    var course: Course?
    
    var holeArray = [Hole]()
    var scoreDict = [Player: [Int]]()
    var playerArray = [Player]()
    var currentPlayer: Player?
    var oldStepperValue: Int?
    var currentPlayerView: UIView?
    
    var thisScore = 0
    
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        parTextField?.delegate = self
        //collectionView?.backgroundColor = ColorPalette.darkColor.color
        for player in playerArray {
            scoreDict[player] = [Int](count: numHoles, repeatedValue: 0)
            totScore[player] = 0
        }
        
        
        currentPlayer = playerArray.first
        courseNameLabel?.text = course?.name
        
        
        self.configureCourse()
        
        self.initScoreArray()
        
        self.configureView()
        
        makeSubview()

        collectionView?.reloadData()
    }
    
    func makeSubview() {
        let rect = CGRect(x: 117.5, y: 55, width: 40, height: 40)
        currentPlayerView = UIView(frame: rect)
        currentPlayerView!.layer.cornerRadius = 10.0
        currentPlayerView!.backgroundColor = ColorPalette.blueColor.color
        currentPlayerView?.backgroundColor = UIColor(red: 74/255.0, green: 144/255.0, blue: 226/255.0, alpha: 0.3)
        collectionView?.insertSubview(currentPlayerView!, atIndex: 0)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureCourse() {
        let thisCourse = course!
        
        holeArray = thisCourse.holes?.allObjects as! [Hole]
        
        sortHoles()
        holeArray = Array(holeArray[(beginOnHole - 1)...(numHoles + beginOnHole - 2)])
        
    }

    
    func prevHole() {
        UIView.animateWithDuration(0.25, animations: {
            self.currentPlayerView!.center.x -= 55
            //self.currentPlayerView!.center.y = 65
        })
        holeNumber--
        configureView()
        
        // Scroll to correct position
        let indexPath = NSIndexPath(forItem: holeNumber - 1, inSection: playerArray.indexOf(currentPlayer!)!)
        collectionView?.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
        
    }
    
    func computeParDiff() {
        for player in playerArray {
            parDiff[player] = 0
            for score in scoreDict[player]!.enumerate() {
                if didPlayHole[player]![score.index] == true {
                    parDiff[player]! += score.element - Int(holeArray[score.index].par!)
                }
                
            }
            
        }
    }
    
    func initScoreArray() {
        for player in playerArray {
            didPlayHole[player] = [Bool]()
            
            for hole in holeArray {
                scoreDict[player]?[Int(hole.number!) - beginOnHole] = Int(hole.par!)
                didPlayHole[player]?.append(false)
            }
            
            didPlayHole[playerArray.first!]?[0] = true
            //totScore[player] = scoreDict[player]![0]
            parDiff[player] = 0
        }
        totScore[playerArray.first!] = scoreDict[playerArray.first!]![0]
        
        
    }
    
    func configureView() {
        
        
        parTextField?.hidden = true
        addParButton?.hidden = true
        
        //print(playerArray.description)
        headerLabel?.text = "\(holeArray[holeNumber-1].number!) av \((holeArray.last?.number)!)"
        
        parTextField?.placeholder = "Par for hull \(holeNumber)"
        
        if let curPlay = currentPlayer {
            if scoreDict[curPlay]?.count >= holeNumber {
                scoreLabel?.text = "\((scoreDict[curPlay]?[holeNumber-1]))"
            }
            //print(scoreArray[holeNumber-1])
            scoreStepper?.value = Double((scoreDict[curPlay]![holeNumber - 1]))
            oldStepperValue = Int((scoreStepper?.value)!)
            scoreLabel?.text = Int(scoreStepper!.value).description
            userButton?.setTitle(curPlay.name, forState: .Normal)
            //totalScoreLabel?.text = "\(totScore[curPlay]!)"
            
            computeParDiff()
            
            if holeArray[holeNumber-1].parDefined == true {
                parLabel?.text = "Par: \(holeArray[holeNumber-1].par!)"
            } else {
                parLabel?.text = "Par: 3 (udefinert)"
            }
            
            if holeArray[holeNumber - 1].locked == 1 {
                changeParButton?.hidden = true
                addParButton?.hidden = true
            } else {
                self.changeParButton?.hidden = false
            }
            
            if let curPlayView = currentPlayerView {
                UIView.animateWithDuration(0.25, animations: {
                    curPlayView.center.y = CGFloat(50 * Int(self.playerArray.indexOf(self.currentPlayer!)!) + 75)
                    curPlayView.center.x = CGFloat(55 * (self.holeNumber - 1) + 137)
                })
            }
            
            
            
        }
        
        
        
        
        let progress = Float(holeNumber) / Float(holeArray.count)
        progressBar?.setProgress(progress, animated: true)
        
        //print("\(holeNumber.description) == \(holeArray.last?.number?.description)")
        if holeNumber == Int((holeArray.last?.number)!) - beginOnHole + 1 && currentPlayer == playerArray.last {
            nextHoleButton?.setTitle("Ferdig", forState: .Normal)
        } else {
            nextHoleButton?.setTitle("Neste", forState: .Normal)
        }
        
        collectionView?.reloadData()
        
        
    }
    
    func defineCourse(course: Course) {
        self.course = course
    }
    
    
    @IBAction func nextButtonPressed(sender: AnyObject) {
        //let indexPath = NSIndexPath(forItem: 1, inSection: 0)
        //let cell = collectionView!.dequeueReusableCellWithReuseIdentifier("playerCell", forIndexPath: indexPath) as! PlayerCollectionViewCell
        
        /*UIView.animateWithDuration(0.5, animations: {
            self.userButton?.center.x += self.view.bounds.width
            self.userButton?.center.y -= self.view.bounds.height
        })*/
        
        
        
        if holeNumber == Int((holeArray.last?.number)!) - beginOnHole + 1 && currentPlayer == playerArray.last {
            performSegueWithIdentifier("showSummary", sender: self)
        } else {
            if currentPlayer == playerArray.last {
                
                // Update total score dict
                
                if !didPlayHole[playerArray.first!]![holeNumber] {
                    totScore[playerArray.first!]! += scoreDict[playerArray[playerArray.indexOf(currentPlayer!)!]]![holeNumber]
                }
                
                didPlayHole[currentPlayer!]?[holeNumber - 1] = true

                currentPlayer = playerArray.first
                
                // Go to next hole
                nextHole()
                
                
            } else  {
                
                // Update total score dict
                if !didPlayHole[playerArray[playerArray.indexOf(currentPlayer!)! + 1]]![holeNumber - 1] {
                    totScore[playerArray[playerArray.indexOf(currentPlayer!)! + 1]]! += scoreDict[playerArray[playerArray.indexOf(currentPlayer!)! + 1]]![holeNumber - 1]
                }
                
                // Go to next player
                nextPlayer()
            }
            
        }
    }
    
    func didUpdateStepper() {
        oldStepperValue = Int((scoreStepper?.value)!)
    }

    @IBAction func scoreStepperChanged(sender: UIStepper) {
        if let curPlay = currentPlayer {
            scoreDict[curPlay]?[holeNumber - 1] = Int(sender.value)
            
            scoreLabel?.text = Int(sender.value).description
            
            if Int(sender.value) > oldStepperValue {
                (totScore[curPlay]!)++
                (parDiff[curPlay]!)++
            } else {
                (totScore[curPlay]!)--
                (parDiff[curPlay]!)--
            }
            //totalScoreLabel?.text = "\(totScore[curPlay]!)"
        }
        
        collectionView?.reloadData()
        didUpdateStepper()
        
    }
    
    @IBAction func prevButtonPressed(sender: AnyObject) {
        if holeNumber > 1 {
            prevHole()
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.hidden = true
        return true
    }
    
    func nextPlayer() {
        
        // Scroll to correct position
        let indexPath = NSIndexPath(forItem: holeNumber, inSection: playerArray.indexOf(currentPlayer!)! + 2)
        collectionView?.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.Bottom.union(.Right), animated: true)
        
        UIView.animateWithDuration(0.25, animations: {
            self.currentPlayerView!.center.y += 50
        })
        
        
        if let curPlay = currentPlayer {
            currentPlayer = playerArray[playerArray.indexOf(curPlay)! + 1]
        }
        
        didPlayHole[currentPlayer!]?[holeNumber - 1] = true
        
        configureView()
        
    }
    
    func nextHole() {
        
        // Scroll to correct position
        let indexPath = NSIndexPath(forItem: holeNumber + 1, inSection: playerArray.indexOf(currentPlayer!)!)
        collectionView?.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.Right.union(.Bottom), animated: true)
        
        
        /*if !didPlayHole[currentPlayer!]![holeNumber - 1] {
            totScore[playerArray.first!]! += scoreDict[playerArray.first!]![holeNumber]
        }*/
        
        UIView.animateWithDuration(0.25, animations: {
            self.currentPlayerView!.center.y -= CGFloat(50 * Int(self.playerArray.count - 1))
            self.currentPlayerView!.center.x += 55
        })
        
        
        //print(didPlayHole[currentPlayer!]?.description)
        
        
        holeNumber++
        
        didPlayHole[currentPlayer!]?[holeNumber - 1] = true
        
        configureView()
    }
    
    func sortHoles() {
        holeArray = holeArray.sort({Int($0.number!) < Int($1.number!)})
    }
    
    
    func didSaveGame() {
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
        
        if indexPath.item == 0 && indexPath.section == 0 {
            return
        }
        else if indexPath.item == 0 {
            
            // Update total score dict
            if !didPlayHole[playerArray[indexPath.section - 1]]![holeNumber - 1] {
                totScore[playerArray[indexPath.section - 1]]! += scoreDict[playerArray[indexPath.section - 1]]![holeNumber - 1]
            }
            
            currentPlayer = playerArray[indexPath.section - 1]
            didPlayHole[currentPlayer!]![holeNumber - 1] = true
            
            
            
        } else if indexPath.section == 0 {
            // Update total score dict
            if !didPlayHole[currentPlayer!]![indexPath.item - 1] {
                totScore[currentPlayer!]! += scoreDict[currentPlayer!]![indexPath.item - 1]
            }
            holeNumber = indexPath.item
            didPlayHole[currentPlayer!]![holeNumber - 1] = true
        } else {
            // Update total score dict
            if !didPlayHole[playerArray[indexPath.section - 1]]![indexPath.item - 1] {
                totScore[playerArray[indexPath.section - 1]]! += scoreDict[playerArray[indexPath.section - 1]]![indexPath.item - 1]
            }
            currentPlayer = playerArray[indexPath.section - 1]
            holeNumber = indexPath.item
            didPlayHole[currentPlayer!]![holeNumber - 1] = true
            
        }
        
        
        
        configureView()
        
        NSLog("Trykket på celle")
    }
    @IBAction func changeParButtonPressed(sender: AnyObject) {
        addParButton?.hidden = false
        parTextField?.hidden = false
        changeParButton?.hidden = true
        parTextField?.becomeFirstResponder()
    }
    
    @IBAction func addParButtonPressed(sender: AnyObject) {
        if parTextField?.text != "" {
            
            //changeParButton?.hidden = false
            addParButton?.hidden = true
            let urlString = "http://pthaga.no/frisbee/setParForCourse.php"
            
            Alamofire.request(.GET, urlString, parameters: ["name": (course?.name!)!, "holeNumber": holeNumber, "par": (parTextField?.text)!])
            
            NSLog("newPar defined!")
            parTextField!.hidden = true
            holeArray[holeNumber - 1].setValue(Int((parTextField?.text)!), forKey: "par")
            holeArray[holeNumber - 1].setValue(true, forKey: "parDefined")
            
            parTextField?.text = ""
            parTextField?.resignFirstResponder()
            configureView()
            
        } else {
            if parTextField!.hidden == true {
                parTextField!.hidden = true
                addParButton?.hidden = false
            } else {
                parTextField!.hidden = false
                addParButton?.hidden = true
                parTextField?.hidden = true
            }
        }
    }
    
    
    func getParDiff(forPlayer player: Player) -> String {
        var prefix: String {
            if parDiff[player] > 0 {
                return "+"
            } else if parDiff[player] <= 0 {
                return "-"
            } else {
                return ""
            }
        }
        
        
        return prefix + abs(parDiff[player]!).description
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 && indexPath.section > 0 {
            // Player cell
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("playerCell", forIndexPath: indexPath) as! PlayerCollectionViewCell
            
            if playerArray[Int(indexPath.section) - 1] == currentPlayer! {
                cell.playerView?.backgroundColor = ColorPalette.blueColor.color
            } else {
                cell.playerView?.backgroundColor = ColorPalette.grayColor.color
            }
            
            cell.playerLabel?.text = playerArray[Int(indexPath.section) - 1].name
            cell.totalScore?.text = totScore[playerArray[indexPath.section - 1]]?.description
            
            var sign = ""
            
            
            if parDiff[playerArray[indexPath.section - 1]] > 0 {
                sign = "+"
            }
            
            cell.parDiff?.text = "(\(sign)\(parDiff[playerArray[indexPath.section - 1]]!.description))"
            
            if parDiff[playerArray[indexPath.section - 1]] > 0 {
                cell.parDiff?.textColor = UIColor.redColor()
            } else {
                cell.parDiff?.textColor = ColorPalette.greenColor.color
            }
            
            return cell
        } else if indexPath.item > 0 && indexPath.section == 0 {
            
            // hole numbers
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
            cell.hidden = false
            cell.textLabel?.text = "\(indexPath.item)"
            cell.backgroundColor = ColorPalette.viewBackGroundColor.color
            cell.textLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 16.0)

            
            return cell
        } else if indexPath.section == 0 && indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("playerCell", forIndexPath: indexPath) as! PlayerCollectionViewCell
            cell.playerLabel?.text = nil
            cell.totalScore?.text = nil
            cell.parDiff?.text = nil

            cell.backgroundColor = ColorPalette.viewBackGroundColor.color
            cell.playerView?.backgroundColor = UIColor.clearColor()
                //cell.textLabel?.text = nil
            return cell
        } else {
            
            // Score numbers
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
            
            if didPlayHole[playerArray[indexPath.section - 1]]![indexPath.item - 1] == true {
                cell.hidden = false
                cell.textLabel?.text = "\(scoreDict[playerArray[indexPath.section - 1]]![indexPath.item - 1])"
            }
            
            if playerArray[Int(indexPath.section) - 1] == currentPlayer! && holeNumber == indexPath.item  {
                //cell.textView?.backgroundColor = ColorPalette.blueColor.color
                //cell.textLabel?.textColor = UIColor.whiteColor()
            } else {
                //cell.textView?.backgroundColor = ColorPalette.lightColor.color
                cell.textLabel?.textColor = UIColor.blackColor()
            }
            
            
            
            return cell
        }
        
        
        
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numHoles + 2
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return playerArray.count + 1
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //let navigationController = segue.destinationViewController as! UINavigationController
        //let destination = navigationController.topViewController as! SummaryViewController
        let destination = segue.destinationViewController as! SummaryViewController
        
        destination.holeArray = holeArray
        destination.scoreDict = scoreDict
        destination.playerArray = playerArray
        destination.course = course
        destination.parDiff = self.parDiff
        
        //destination.delegate = self
        
    }
    

}
