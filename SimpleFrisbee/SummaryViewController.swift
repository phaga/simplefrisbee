//
//  SummaryViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 05.02.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

class SummaryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource {
    var holeArray = [Hole]()
    var scoreDict = [Player: [Int]]()
    var playerArray  = [Player]()
    var parDiff = [Player: Int]()
    var course: Course?
    var showDetailPlayer: Player?
    var avgPerHole = [Player: Double]()
    var totalForPrevGame = [Player: Int]()
    
    @IBOutlet weak var detailView: PlayView?
    @IBOutlet weak var detailTableView: UITableView?
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    
    @IBOutlet weak var playView: PlayView?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var playerNameLabel: UILabel?
    @IBOutlet weak var detailCourseLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .None
        detailView?.hidden = true
        collectionView?.delegate = self
        collectionView?.dataSource = self
        detailTableView?.delegate = self
        detailTableView?.dataSource = self
        //collectionHeightConstraint?.constant = CGFloat((playerArray.count + 3)*50)
        //collectionHeightConstraint?.priority = 1000
        showDetailPlayer = playerArray.first!
        configureDetails()
        
        let b = UIBarButtonItem(title: "Lagre", style: UIBarButtonItemStyle.Done, target: self, action: "saveGame")
        self.navigationItem.rightBarButtonItem = b
        
        self.title = "Oppsummering"
        
        
        playerArray = playerArray.sort({computeTotal(forPlayer: $0) < computeTotal(forPlayer: $1)})
        
        
        showDetail(forPlayer: playerArray.first!)
        
        collectionView?.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func findPreviousGameOnThisCourse(forPlayer player: Player) -> Game? {
        let games = player.games?.allObjects as! [Game]
        
        var gamesOnThisCourse = [Game]()
        
        for game in games {
            if game.course == course {
                gamesOnThisCourse.append(game)
            }
        }
        
        if gamesOnThisCourse.count == 0 {
            return nil
        }
        
        gamesOnThisCourse = gamesOnThisCourse.sort({(($0.date! as NSDate).timeIntervalSince1970) > ($1.date! as NSDate).timeIntervalSince1970})
        
        
        NSLog((gamesOnThisCourse.first?.date?.description)!)
        
        return gamesOnThisCourse.first
        
        
    }
    
    func toggleShowDetail() {
        let slideInFromLeftTransition = CATransition()
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromBottom
        slideInFromLeftTransition.duration = 0.3
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        // Add the animation to the View's layer
        detailView!.layer.addAnimation(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        
        if detailView?.hidden == true {
            detailView?.hidden = false
        } else {
            detailView?.hidden = true
        }
    }
    
    func showDetail(forPlayer player: Player) {
        
        
        if detailView?.hidden == true || player != showDetailPlayer {
            
            showDetailPlayer = player
            
            let slideInFromLeftTransition = CATransition()
            // Customize the animation's properties
            slideInFromLeftTransition.type = kCATransitionPush
            slideInFromLeftTransition.subtype = kCATransitionFromBottom
            slideInFromLeftTransition.duration = 0.3
            slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            slideInFromLeftTransition.fillMode = kCAFillModeRemoved
            // Add the animation to the View's layer
            detailView!.layer.addAnimation(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
            playerNameLabel?.text = showDetailPlayer?.name
            detailCourseLabel.text = course?.name!
            detailView?.hidden = false
        }
        
        
        detailTableView?.reloadData()
    }
    
    func computeTotal(forPlayer player: Player) -> Int? {
        guard let scores = scoreDict[player] else {
            return nil
        }
        var tot = 0
        for score in scores {
            tot += score
        }
        
        return tot
    }
    
    func computeTotalPrevGame(forPlayer player: Player, prevGame: Game) -> Int {
        var tot = 0
        let prevGameScores = findPreviousGameOnThisCourse(forPlayer: player)!.score?.allObjects as! [Score]
        
        for score in prevGameScores {
            if score.player == player {
                tot += Int(score.score!)
            }
        }
        
        return tot
    }
    
    func configureDetails() {
        for player in playerArray {
            if let totalScore = computeTotal(forPlayer: player) {
                avgPerHole[player] = Double(totalScore) / Double(holeArray.count)
                
            }
            let prevGame = findPreviousGameOnThisCourse(forPlayer: player)
            
            if let previousGame = prevGame {
                totalForPrevGame[player] = computeTotalPrevGame(forPlayer: player, prevGame: previousGame)
            }
            
        }
        
    }
    
    
    func saveGame() {
        let gameEntity = NSEntityDescription.insertNewObjectForEntityForName("Game", inManagedObjectContext: managedContext) as! Game
        
        let thisDate = NSDate()
        let players = NSSet(array: playerArray)
        gameEntity.setValue(players, forKey: "players")
        gameEntity.setValue(thisDate, forKey: "date")
        gameEntity.setValue(course, forKey: "course")
        gameEntity.setValue(holeArray.count, forKey: "numHoles")
        
        
        for player in playerArray {
            var i = 1
            for score in scoreDict[player]! {
                let scoreEntity = NSEntityDescription.insertNewObjectForEntityForName("Score", inManagedObjectContext: managedContext) as! Score
                scoreEntity.setValue(score, forKey: "score")
                scoreEntity.setValue(i, forKey: "holeNr")
                scoreEntity.setValue(gameEntity, forKey: "game")
                scoreEntity.setValue(holeArray[i-1], forKey: "hole")
                scoreEntity.setValue(player, forKey: "player")
                
                i++
            }
        }
        

        
        let historyEntity = NSEntityDescription.insertNewObjectForEntityForName("History", inManagedObjectContext: managedContext) as! History
        
        historyEntity.setValue(thisDate, forKey: "date")
        historyEntity.setValue(course, forKey: "course")
        historyEntity.setValue(gameEntity, forKey: "game")
        
        
        do {
            try managedContext.save()
        } catch {
            fatalError("Fatal Error!")
        }
        
        navigationController?.popToRootViewControllerAnimated(true)
        
    }
    
    // MARK: - Collection Source functions
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return holeArray.count + 5
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return playerArray.count + 1
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.item == 0 && indexPath.section > 0 {
            
            showDetail(forPlayer: playerArray[indexPath.section - 1])
            collectionView.reloadData()
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 && indexPath.section == 0 {
            // Player cell
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
            cell.backgroundColor = ColorPalette.viewBackGroundColor.color
            cell.textLabel?.text = "Spiller"
            
            return cell
        } else if indexPath.item == 0 && indexPath.section > 0 {
            // Player cell
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("playerCell", forIndexPath: indexPath) as! PlayerCollectionViewCell
            
            if showDetailPlayer == playerArray[Int(indexPath.section) - 1] {
                cell.playerView?.backgroundColor = ColorPalette.blueColor.color
            } else {
                cell.playerView?.backgroundColor = ColorPalette.grayColor.color
            }
            
            
            cell.playerLabel?.text = playerArray[Int(indexPath.section) - 1].name!
            
            return cell
        } else if indexPath.item == 1 && indexPath.section == 0 {
            
            // Total label
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
            cell.textLabel?.text = "Total"
            cell.textLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 10.5)
            cell.backgroundColor = ColorPalette.viewBackGroundColor.color
            return cell
            
            
        } else if indexPath.item > 1 && indexPath.section == 0 {
            
            // Hole number label
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
            cell.textLabel?.text = (indexPath.item - 1).description
            cell.textLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 16.0)
            cell.backgroundColor = ColorPalette.viewBackGroundColor.color
            return cell
            
        }  else if indexPath.item == 1 && indexPath.section > 0 {
            
            // Print totals for each player
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
            cell.textLabel?.text = computeTotal(forPlayer: playerArray[indexPath.section - 1])?.description
            cell.textLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 16.0)
            return cell
            
        } else {
            
            // Print score for each player
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
            //cell.textLabel?.text = ""
            let player = playerArray[indexPath.section - 1]
            cell.textLabel?.text = scoreDict[player]![indexPath.item - 2].description
            
            let par = holeArray[indexPath.section - 1].par
            
            let parDiff = scoreDict[player]![indexPath.item - 2] - Int(par!)
            
            if parDiff <= 0 {
                cell.textLabel?.textColor = ColorPalette.greenColor.color
            } else if parDiff == 1 {
                cell.textLabel?.textColor = UIColor.orangeColor()
            } else if parDiff > 1 {
                cell.textLabel?.textColor = UIColor.redColor()
            }
            
            
            return cell
        }
        
        
    }
    
    // MARK: - Table data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var descriptionText = ""
        var rightDetail = ""
        
        switch (indexPath.row) {
        case 0:
            descriptionText = "Gjennomsnitt per kurv"
            rightDetail = String(format: "%.2f", avgPerHole[showDetailPlayer!]!)
        case 1:
            descriptionText = "Forrige spill på denne banen"
            if let totForPrevGame = totalForPrevGame[showDetailPlayer!] {
                rightDetail = totForPrevGame.description
            } else {
                rightDetail = " - "
            }
        case 2:
            
            descriptionText = "         differanse"
            
            if let totForPrevGame = totalForPrevGame[showDetailPlayer!] {
                rightDetail = (computeTotal(forPlayer: showDetailPlayer!)! - totForPrevGame).description
            } else {
                rightDetail = " - "
            }
            
        case 3: descriptionText = "Gjennomsnitt på denne banen"
        case 4: descriptionText = "         differanse"
        
        default:
            descriptionText = ""
            rightDetail = ""
        }
        let cell = detailTableView?.dequeueReusableCellWithIdentifier("detailCell")
        cell?.textLabel?.text = descriptionText
        cell?.detailTextLabel?.text = rightDetail
        
        return cell!
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
