//
//  GameSettingsTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 06.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

class GameSettingsTableViewController: UITableViewController, ChoosePlayersTableViewControllerDelegate {
    @IBOutlet weak var courseNameLabel: UILabel?
    @IBOutlet weak var numberOfHolesSeg: UISegmentedControl?
    @IBOutlet weak var beginOnHoleSeg: UISegmentedControl?
    @IBOutlet weak var numSelectedPlayersLabel: UILabel?
    @IBOutlet weak var startButton: UIButton?
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }

    
    var selectedPlayers = [Player]()
    
    var course: Course? {
        didSet {
            configureView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        numberOfHolesSeg?.addTarget(self, action: "numberOfHolesSegmentChange:", forControlEvents: .AllEvents)
        
        let downloadManager = DownloadManager()
        
        downloadManager.getPar(forCourse: self.course!)
        
        if let me = loadMe() {
            selectedPlayers.append(me)
            didChangePlayers()
        }
        
        configureView()
        
    }
    
    func configureView() {
        
        courseNameLabel?.text = course?.name
        
        guard let thisCourse = course else {
            fatalError("Course was nil")
        }
        
        guard let numHoles = thisCourse.numHoles as? Int else {
            fatalError("Could not find the number of holes.")
        }
        
        if selectedPlayers.count < 1 || numHoles < 1 {
            startButton?.enabled = false
        } else {
            startButton?.enabled = true
        }
        
        
        numberOfHolesSeg?.setTitle("\(min(9, numHoles))", forSegmentAtIndex: 0)
        numberOfHolesSeg?.setEnabled(numHoles > 9, forSegmentAtIndex: 1)
        
        if numHoles > 9 {
            numberOfHolesSeg?.setTitle("\(numHoles)", forSegmentAtIndex: 1)
        }
        
        beginOnHoleSeg?.setEnabled(numHoles > 9, forSegmentAtIndex: 1)
        beginOnHoleSeg?.setTitle("\(max(numHoles - 8, 1))", forSegmentAtIndex: 1)
        tableView.reloadData()
        
    }


    func numberOfHolesSegmentChange(segment: UISegmentedControl) {
        if segment.selectedSegmentIndex == 1 {
            beginOnHoleSeg?.selectedSegmentIndex = 0
            beginOnHoleSeg?.setEnabled(false, forSegmentAtIndex: 1)
        } else {
            beginOnHoleSeg?.setEnabled(true, forSegmentAtIndex: 1)
        }
    }
    
    func loadMe() -> Player? {
        let playerFetch = NSFetchRequest(entityName: "Me")
        
        do {
            let fetchedMe = try managedContext.executeFetchRequest(playerFetch) as! [Me]
            let me = fetchedMe.first
            if let thisMe = me {
                return (thisMe.player)!
            } else {
                return nil
            }
        } catch {
            fatalError("Bad things happened")
        }
    }
    
    func didChangePlayers() {
        configureView()
        numSelectedPlayersLabel?.text = "\(selectedPlayers.count.description) spillere valgt"
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "startGame" {
            let destination = segue.destinationViewController as! PlayViewController
            
            if let thisCourse = course {
                destination.defineCourse(thisCourse)
            } else {
                fatalError("Something wrong")
            }
            
            if let numberOfHolesIndex = numberOfHolesSeg?.selectedSegmentIndex, let beginOnHoleIndex = beginOnHoleSeg?.selectedSegmentIndex {
                if let chosenNumHoles = Int((numberOfHolesSeg?.titleForSegmentAtIndex(numberOfHolesIndex))!),
                    let chosenBeginOnHole = Int((beginOnHoleSeg?.titleForSegmentAtIndex(beginOnHoleIndex))!) {
                    destination.numHoles = chosenNumHoles
                    destination.beginOnHole = chosenBeginOnHole
                }
                
            }
            
            destination.playerArray = selectedPlayers
        }
        else if segue.identifier == "choosePlayers"{
            let destination = segue.destinationViewController as! ChoosePlayersTableViewController
            destination.selectedPlayers = selectedPlayers
            destination.delegate = self
        }
        
        
    }
    

}
