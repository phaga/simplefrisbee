//
//  HistoryTableViewCell.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 14.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    @IBOutlet weak var totalScoreLabel: UILabel?
    @IBOutlet weak var totalParDiffLabel: UILabel?
    @IBOutlet weak var courseNameLabel: UILabel?
    @IBOutlet weak var dateLabel: UILabel?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
