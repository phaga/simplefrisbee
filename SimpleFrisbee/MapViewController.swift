//
//  MapViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 01.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol MapViewControllerDelegate {
    func didSavePosition(position: CLLocation)
}

class MapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView?
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var delegate: MapViewControllerDelegate?
    var position: CLLocation?
    
    
    override func viewDidLoad() {
        saveButton.enabled = false
        super.viewDidLoad()
        
        let locationManager = CLLocationManager()
        
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.delegate = self
        mapView?.delegate = self
        
        if let pos = position {
            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: pos.coordinate.latitude, longitude: pos.coordinate.longitude))
            marker.map = self.mapView
            let camera = GMSCameraPosition.cameraWithLatitude(pos.coordinate.latitude,
                longitude: pos.coordinate.longitude, zoom: 10)
            self.mapView?.camera = camera
            saveButton.enabled = true
            
        }
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func mapView(mapView: GMSMapView!, didLongPressAtCoordinate coordinate: CLLocationCoordinate2D) {
        self.mapView!.clear()
        let marker = GMSMarker(position: coordinate)
        marker.map = self.mapView
        saveButton.enabled = true
        self.position = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        print("Save button pressed.")
        if let pos = self.position {
            print("Defined positon!")
            print(delegate)
            delegate?.didSavePosition(pos)
        }
    }
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

}
