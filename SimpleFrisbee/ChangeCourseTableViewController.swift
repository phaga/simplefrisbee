//
//  ChangeCourseTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 10.02.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

protocol ChangeCourseTableViewControllerDelegate {
    func didChangeCourse()
}

class ChangeCourseTableViewController: UITableViewController, MapViewControllerDelegate, AddHoleTableViewControllerDelegate, TextFieldTableViewCellDelegate {
    
    var holes = [NSManagedObject]()
    var totalPar = 0
    var totalLength = 0.0
    var delegate: ChangeCourseTableViewControllerDelegate?
    var position: CLLocation?
    var courseName: String?
    
    
    var holeDict = [Int: Int]() {
        didSet {
            computeTotalPar()
        }
    }
    
    var lengthDict = [Int: Double]() {
        didSet {
            //computeTotalLength()
        }
    }
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    var course: Course?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCourse()
        tableView.reloadData()
    }
    
    func configureCourse() {
        let holeArray = course!.holes?.allObjects as! [Hole]
        
        for hole in holeArray {
            holeDict[Int(hole.number!)] = Int(hole.par!)
        }
        
        // Setting name
        if let nameText = course?.name {
            courseName = nameText
        }
        position = CLLocation(latitude: CLLocationDegrees((course?.lat)!), longitude: CLLocationDegrees((course?.long)!))
    }
    
    
    func computeTotalPar() {
        for value in holeDict.values {
            self.totalPar += value
        }
    }
    
    func computeTotalLength() {
        for value in lengthDict.values {
            self.totalLength += value
        }
    }
    
    func saveCourse() {
        
        guard let name = courseName else {
            print("Could not find name")
            return
        }
        
        course!.setValue(name, forKey: "name")
        
        if let pos = self.position {
            let lat = pos.coordinate.latitude
            let long = pos.coordinate.longitude
            course!.setValue(lat, forKey: "lat")
            course!.setValue(long, forKey: "long")
        }
        
        course!.setValue(holeDict.count, forKey: "numHoles")
        
        print(holeDict.count)
        
        // Setting par and length
        for key in holeDict.keys {
            let holeEntity = NSEntityDescription.insertNewObjectForEntityForName("Hole", inManagedObjectContext: managedContext) as! Hole
            holeEntity.setValue(holeDict[key], forKey: "par")
            if let length = lengthDict[key] {
                holeEntity.setValue(length, forKey: "length")
            }
            holeEntity.setValue(key, forKey: "number")
            holeEntity.course = course!
        }
        
        
        do {
            try managedContext.save()
        } catch {
            fatalError("Fatal Error!")
        }
        
        
        
    }
    
    func didChangeTextField(name: String) {
        self.courseName = name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSavePosition(position: CLLocation) {
        dismissViewControllerAnimated(true, completion: nil)
        
        self.position = position
        
        self.tableView.reloadData()
    }
    
    func didAddHole(hole: Int, par: Int, length: Double?) {
        self.holeDict.updateValue(par, forKey: hole)
        
        if let len = length {
            self.lengthDict.updateValue(len, forKey: hole)
        }
        
        print(holeDict)
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1{
            return 1
        } else if section == 2 {
            return holeDict.count + 1
        } else {
            return 0
        }
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("textFieldCell") as! TextFieldTableViewCell
            cell.textField?.text = course?.name
            cell.delegate = self
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("placementCell")!
            if self.position != nil {
                cell.textLabel?.text = "✅Posisjon satt"
                cell.textLabel?.textColor = UIColor.blackColor()
            }
            return cell
        } else if indexPath.section == 2 {
            if indexPath.row == holeDict.count {
                let cell = tableView.dequeueReusableCellWithIdentifier("addHoleCell")!
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCellWithIdentifier("holeCell") as! HoleTableViewCell
                if let thisHole = holeDict[indexPath.row + 1] {
                    cell.holeNrLabel?.text = "\(indexPath.row + 1)"
                    cell.parLabel?.text = "Par \(thisHole)"
                    if let length = lengthDict[indexPath.row + 1] {
                        cell.lengthLabel?.text = "\(length) m"
                    } else {
                        cell.lengthLabel?.text = " - "
                    }
                }
                return cell
            }
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("holeCell")!
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch(section) {
        case 0: return "Navn"
        case 1: return "Sted"
        case 2: return "Hull"
        default: return nil
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 1 {
            performSegueWithIdentifier("showMap", sender: nil)
        }
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        saveCourse()
        delegate?.didChangeCourse()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showMap" {
            let destination = segue.destinationViewController as! UINavigationController
            let addPositionTableViewController = destination.topViewController as! MapViewController
            print("show map!")
            addPositionTableViewController.delegate = self
            addPositionTableViewController.position = CLLocation(latitude: CLLocationDegrees((course?.lat)!), longitude: CLLocationDegrees((course?.long)!))
            
        } else if segue.identifier == "addHole" {
            let destination = segue.destinationViewController as? AddHoleTableViewController
            destination?.delegate = self
            destination?.holeNumber = holeDict.count + 1
            
        }
    }

}
