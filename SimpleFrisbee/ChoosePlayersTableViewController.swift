//
//  ChoosePlayersTableViewController.swift
//  SimpleFrisbee
//
//  Created by Per Thomas Haga on 14.01.2016.
//  Copyright © 2016 Per Thomas Haga. All rights reserved.
//

import UIKit
import CoreData

protocol ChoosePlayersTableViewControllerDelegate {
    var selectedPlayers: [Player] {get set}
    
    func didChangePlayers()
}


class ChoosePlayersTableViewController: UITableViewController {
    
    var managedContext: NSManagedObjectContext {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
        
    }
    
    var players = [Player]()
    var selectedPlayers = [Player]()
    var delegate: ChoosePlayersTableViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        loadPlayers()
        print(players.count.description)
        tableView.reloadData()
        tableView.allowsMultipleSelectionDuringEditing = true
        //self.navigationItem.rightBarButtonItem = self.editButtonItem()
        super.setEditing(true, animated: true)
        tableView.setEditing(true, animated: true)
        configureView()
        
    }
    
    func configureView() {

        for player in selectedPlayers {
            let indexPath = NSIndexPath(forItem: players.indexOf(player)!, inSection: 0)
            tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition(rawValue: 0)!)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadPlayers() {
        let playerFetch = NSFetchRequest(entityName: "Player")
        
        do {
            let fetchedPlayer = try managedContext.executeFetchRequest(playerFetch) as! [Player]
            players = fetchedPlayer
        } catch {
            fatalError("Bad things happened")
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return players.count
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedPlayers.append(players[indexPath.row])
        delegate?.selectedPlayers = selectedPlayers
        delegate?.didChangePlayers()
        
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        selectedPlayers.removeAtIndex(selectedPlayers.indexOf(players[indexPath.row])!)
        delegate?.selectedPlayers = selectedPlayers
        delegate?.didChangePlayers()
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("playerCell", forIndexPath: indexPath)

        cell.textLabel?.text = players[indexPath.row].name

        return cell
    }
    
}
