//
//  Disc+CoreDataProperties.swift
//  
//
//  Created by Per Thomas Haga on 14.01.2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Disc {

    @NSManaged var fade: NSNumber?
    @NSManaged var glide: NSNumber?
    @NSManaged var name: String?
    @NSManaged var speed: NSNumber?
    @NSManaged var turn: NSNumber?
    @NSManaged var type: String?

}
